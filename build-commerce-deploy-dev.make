api = 2
core = 7.x

; Drupal
includes[] = drupal-org-core.make

; Commerce Deploy
projects[commerce_deploy][type] = profile
projects[commerce_deploy][download][type] = git
projects[commerce_deploy][download][branch] = 7.x-1.x
