Commerce Deploy [![Build Status](https://travis-ci.org/CommerceDeploy/commerce_deploy.svg?branch=7.x-1.x)](https://travis-ci.org/CommerceDeploy/commerce_deploy)
===
Commerce Deploy aims to simplify Drupal Commerce development for site builders and developers. Get started faster and cut the cruft of dependency gathering. The ideology behind Commerce Deploy is to provide enhancements to Drupal Commerce development without getting in the way.

###What makes Commerce Deploy different?
Unlike other Drupal Commerce profiles, Commerce Deploy does not have an end goal to provide a specific out-of-the-box goal. Nor does it try to make too many assumptions. The beautiful fact of Commerce is its flexibility, we want to respect that.

####So, what does it do then?
The profile is actually made up of just over a handful of profile related projects that add specific Commerce functionality. The profile itself just adds amenities typical to any Drupal site out in the wild web. This means developers and site builders can reuse components of Commerce Deploy in their own sites or profiles and cut down on their own development time.

##How to build Commerce Deploy

**Requirements**
* [drush](http://drupal.org/project/drush)

**Installation**
* Run `sh build.sh /path/to/install`

**Rebuild Installation**

The profile can be rebuilt from its current installation
* Run `sh rebuild.sh` from profile folder.
