; Commerce Deploy Makefile

api = 2
core = 7.x

; Profile

projects[commerce_deploy_core][version] = 1.x-dev
projects[commerce_deploy_core][subdir] = commerce_deploy
projects[commerce_deploy_core][download][type] = git
projects[commerce_deploy_core][download][branch] = 7.x-1.x
projects[commerce_deploy_core][download][revision] = b8d65f7

projects[commerce_deploy_shipping][version] = 1.x-dev
projects[commerce_deploy_shipping][subdir] = commerce_deploy
projects[commerce_deploy_shipping][download][type] = git
projects[commerce_deploy_shipping][download][branch] = 7.x-1.x
projects[commerce_deploy_shipping][download][revision] = a0a2104

projects[commerce_deploy_promotions][version] = 1.x-dev
projects[commerce_deploy_promotions][subdir] = commerce_deploy
projects[commerce_deploy_promotions][download][type] = git
projects[commerce_deploy_promotions][download][branch] = 7.x-1.x
projects[commerce_deploy_promotions][download][revision] = 6bbc8e8

projects[commerce_deploy_product][version] = 1.x-dev
projects[commerce_deploy_product][subdir] = commerce_deploy
projects[commerce_deploy_product][download][type] = git
projects[commerce_deploy_product][download][branch] = 7.x-1.x
projects[commerce_deploy_product][download][revision] = e75b5f8

projects[commerce_deploy_customer][version] = 1.x-dev
projects[commerce_deploy_customer][subdir] = commerce_deploy
projects[commerce_deploy_customer][download][type] = git
projects[commerce_deploy_customer][download][branch] = 7.x-1.x
projects[commerce_deploy_customer][download][revision] = 158bebe

projects[commerce_deploy_checkout][version] = 1.x-dev
projects[commerce_deploy_checkout][subdir] = commerce_deploy
projects[commerce_deploy_checkout][download][type] = git
projects[commerce_deploy_checkout][download][branch] = 7.x-1.x
projects[commerce_deploy_checkout][download][revision] = 43236d0

projects[commerce_deploy_backend][version] = 1.x-dev
projects[commerce_deploy_backend][subdir] = commerce_deploy
projects[commerce_deploy_backend][download][type] = git
projects[commerce_deploy_backend][download][branch] = 7.x-1.x
projects[commerce_deploy_backend][download][revision] = 61e8497
