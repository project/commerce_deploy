#!/bin/sh
# Script to rebuild the Commerce deploy installation profile
# This command expects to be run within the Commerce deploy profile.
# To use this command you must have `drush make` and `git` installed.

if [ -f drupal-org.make ]; then

  echo "This command can be used to rebuild the installation profile in place."
  echo "  [1] Rebuild profile in place in release mode"
  echo "  [2] Rebuild profile in place in development mode (with .git working-copy)"
  echo "  [3] Rebuild profile in place in development mode (latest dev code with .git working-copy)\n"
  echo "Selection:"
  read SELECTION

  if [ $SELECTION = "1" ]; then

    echo "Rebuilding the install profile..."
    drush make --no-core --no-gitinfofile --contrib-destination=. drupal-org.make

  elif [ $SELECTION = "2" ]; then

    echo "Rebuilding the install profile as a working-copy..."
    drush make --working-copy --no-core --no-gitinfofile --contrib-destination=. drupal-org.make

  elif [ $SELECTION = "3" ]; then

    echo "Building install profile in development mode (latest dev code with .git working-copy)"
    drush make --debug --working-copy --no-core --no-gitinfofile --contrib-destination=. drupal-org.make

  else
   echo "Invalid selection."
  fi
else
  echo 'Could not locate file "drupal-org.make"'
fi
