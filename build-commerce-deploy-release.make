api = 2
core = 7.x

; Drupal
includes[] = drupal-org-core.make

; Commerce Deploy
projects[commerce_deploy][type] = profile
projects[commerce_deploy][version] = 1.0-alpha4
