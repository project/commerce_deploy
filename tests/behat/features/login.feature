Feature: Login to Drupal
  In order to start using additional features of the site
  As an anonymous user
  I should be able to Login

  Scenario: View the Login page
    When I go to "/user/login"
    Then I should see "User account"

  Scenario: Username validation: Invalid username
    When I go to "/user/login"
      And I fill in "Username" with "randomname"
      And I fill in "Password" with "invalidpassword"
      And I press "Log in"
    Then I should see "Sorry, unrecognized username or password."
      And the field "Username" should be outlined in red

  Scenario: User should be able to login and see the user profile
    When I go to "/user/login"
    And I fill in "Username" with "admin"
    And I fill in "Password" with "admin"
    And I press "Log in"
    Then I should see "admin"
    Then I should see the following <links>
      | links                   |
      | View                    |
      | Edit                    |
      | Order history           |
