; Commerce Deploy Makefile

api = 2
core = 7.x

; Profile

projects[commerce_deploy_core][version] = 1.0-alpha4
projects[commerce_deploy_core][subdir] = commerce_deploy

projects[commerce_deploy_shipping][version] = 1.0-alpha4
projects[commerce_deploy_shipping][subdir] = commerce_deploy

projects[commerce_deploy_promotions][version] = 1.0-alpha4
projects[commerce_deploy_promotions][subdir] = commerce_deploy

projects[commerce_deploy_product][version] = 1.0-alpha4
projects[commerce_deploy_product][subdir] = commerce_deploy

projects[commerce_deploy_customer][version] = 1.0-alpha4
projects[commerce_deploy_customer][subdir] = commerce_deploy

projects[commerce_deploy_checkout][version] = 1.0-alpha4
projects[commerce_deploy_checkout][subdir] = commerce_deploy

projects[commerce_deploy_backend][version] = 1.0-alpha4
projects[commerce_deploy_backend][subdir] = commerce_deploy
