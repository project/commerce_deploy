; Commerce Deploy Makefile

api = 2
core = 7.x

; Profile

projects[commerce_deploy_core][download][type] = git
projects[commerce_deploy_core][download][branch] = 7.x-1.x
projects[commerce_deploy_core][subdir] = commerce_deploy

projects[commerce_deploy_shipping][download][type] = git
projects[commerce_deploy_shipping][download][branch] = 7.x-1.x
projects[commerce_deploy_shipping][subdir] = commerce_deploy

projects[commerce_deploy_promotions][download][type] = git
projects[commerce_deploy_promotions][download][branch] = 7.x-1.x
projects[commerce_deploy_promotions][subdir] = commerce_deploy

projects[commerce_deploy_product][download][type] = git
projects[commerce_deploy_product][download][branch] = 7.x-1.x
projects[commerce_deploy_product][subdir] = commerce_deploy

projects[commerce_deploy_customer][download][type] = git
projects[commerce_deploy_customer][download][branch] = 7.x-1.x
projects[commerce_deploy_customer][subdir] = commerce_deploy

projects[commerce_deploy_checkout][download][type] = git
projects[commerce_deploy_checkout][download][branch] = 7.x-1.x
projects[commerce_deploy_checkout][subdir] = commerce_deploy

projects[commerce_deploy_backend][download][type] = git
projects[commerce_deploy_backend][download][branch] = 7.x-1.x
projects[commerce_deploy_backend][subdir] = commerce_deploy
